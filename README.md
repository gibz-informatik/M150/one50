# Shop One50
One50 is a online shop selling fruits, vegetables, herbes and mushrooms to its clients. It is a very simple shop to be used for teaching in module 150 at Gewerblich-industrielles Bildungszentrum Zug (GIBZ).

## Technical Overview
The following points briefly describe some technical aspects of this application.

### PHP
The application is written in PHP and runs on PHP 7.2 or greater.

### PHP Framework _Flow_
This project is based on the PHP framework called _Flow_. This is a feature-rich framework for developing powerful web applications. Please see the online documentation of _Flow_ at [https://flowframework.readthedocs.io/en/stable/index.html](https://flowframework.readthedocs.io/en/stable/index.html).

### MySQL Database
The database powering the online shop is a MySQL database. Its tables are defined as simple model classes and converted to MySQL database tables using [Doctrine](https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/index.html) as object relational mapper (ORM). This concept sometimes is referred to as _code first_ (because the database tables are generated according to the model specification in code).

It is important for developers to know, that no direct database manipulations should be done in the database directly. Instead, adopt the model declarations (PHP classes) and then create and execute a doctrine migration.

### Nginx webserver
Nginx is used as simple and lightweight webserver. If you don't tweak the configuration, you may access the shop on your local machine in any browser at [http://localhost:80](http://localhost:80) (you don't need to type the port 80).
The application comes packed with a preconfigured apache webserver.

### Docker
This project is prepared to run with docker. This gives you the advantage to run the PHP application with Nginx webserver locally on your host system as opposed to use the provided virtual machine.

Please note, that the docker installation process is only tested for macOS and Linux at the time of this writing. It is therefore not guaranteed to run smoothly on Windows. Any kind of information and feedback is very welcome! 

For more information about the actual installation process for the dockerized version of this application please see the corresponding [Installation For Docker](./InstallationForDocker.md) document.

## Project structure
This project is structured in two main parts:
1. The actual application is stored in the `/app` directory. Of primary interest are three _Flow_ packages located at `app/Packages/Application`:
	- The package `One50.Shop` contains all files which are required for running the e-shop.
	- The package `One50.DemoData` contains a collection of dummy products with title, description, random price, images and other attributes. This package is used only to pre-fill the shop with some sample products. You don't really need this package if you'd like to enter your own products and categories.
	- The package `One50.TwoFactorAuth` is actually empty (apart from the `composer.json` file). This is the place where you are to put your own code and configuration while working on this project.
1. In the `VM` directory are some files which are required during the installation process in the provided virtual machine.
1. The docker configuration consists of the file `docker-compose.yaml` located in the root directory. All other configuration and resources for docker are located in the `/Docker` directory. 
 

## Installation Process
### Prerequisites
Before you start with the installation process as described bellow, please make sure to meet all prerequisites:

#### Gitlab
You should have access to Gitlab (which most likely is the case while you're reading this). Furthermore you should create an _Access Token_ on Gitlab.

Follow the instructions on [https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) to create an Access Token. While you won't need activated all scopes, it's safe to check all available options.

**Write down the access token after generation.** You won't be able to recover this token. If you lose the token, you'll have to revoke it and generate a new one!

#### Virtual machine
The following installation instructions are based on the assumption you have the provided virtual machine ready and running.

Here are some relevant information for this virtual machine:
- username: `one50`
- password: `m150`
- sudo password: `m150`
- mysql password: `m150` 

### Installation
Your are supposed to run this application in a virtual machine running Ubuntu 18.10. On this machine some required tools are already installed:
- _Nginx_ webserver
- _MySQL_ database server
- Dependency management system _composer_

You'll find a file named `installOne50.sh` in the users home directory of this virtual machine. The only thing to do for you is to start this installation script and provide some required information.

`./installOne50.sh`

Upon startup of this script you'll have to provide some data which is required for the installation process:
- **Gitlab access token**: Please provide your previously created Gitlab access token for cloning your teams git repository
- Information for constructing your git repository url
	- Current **school year**: Provide this information in the form of two two-digit years, divided by a underscore (`_`): YY_YY (example: `18_19`)
	- **Name of your class**: Provide this information all lowercase. Usually you'll use one of the values `infa3a`, `infa3b` or `infwu2`
	- Your **team id**: Your team got an id assigned to by the teacher. Provide this information as two-digit number, potentially with leading zero (example: `05`)
- Your **first name** and **last name** are used for automated creation of two users for the e-shop One50

You might have to provide the _sudo password_ during the installation process. This password is `m150` (as every preconfigured password).

### Start application
After successful installation you'll be able to access to e-shop locally at [http://localhost/](http://localhost/).

Feel free to explore the shop, register additional users or sign in with the automatically created accounts. Their usernames are `customer` and `admin` respectively. Guess their passwords ;-)