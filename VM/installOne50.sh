# Promt all required user information
read -p "Enter the current schoolyear as two two-digit numbers divided by underscore (e.g.: 18_19): " SCHOOLYEAR
read -p "Enter your class name (e.g.: infa3a): " CLASS_NAME
read -p "Enter your team id as two-digit number (e.g.: 05): " TEAM_ID
read -p "Enter your first name: " FIRST_NAME
read -p "Enter your last name: " LAST_NAME

INSTALL_DIR="/var/www/one50.local/"

# Allow access for user one50 to /var/www/
sudo chown -R one50.one50 /var/www/

SSH_KEY_FILE="${HOME}/.ssh/gitlab.key"

# Generate ssh key (if key does not exist already)
if [ ! -f $SSH_KEY_FILE ]; then
    ssh-keygen -t rsa -b 2048 -N "" -f "${HOME}/.ssh/gitlab.key" -C "team${TEAM_ID}@${CLASS_NAME}" -q

    # Print public key
    echo
    echo "Es wurde ein neuer SSH-Key für die Verbindung zu Gitlab erstellt."
    echo "Füge in GitLab unter Settings => SSH Keys den nachfolgenden Public Key hinzu:"
else
    echo
    echo "Es existiert bereits ein SSH-Key für die Verbindung zu Gitlab."
    echo "Stelle sicher, dass dieser Public Key in Gitlab unter Settings => SSH Keys eingetragen ist:"
fi

echo
cat "${HOME}/.ssh/gitlab.key.pub"
echo

echo "Drücke ENTER, wenn der SSH-Key eingetragen ist..."
read -p

# Clone the base repository
cd $INSTALL_DIR
git clone https://git@gitlab.com/gibz-informatik/M150/${SCHOOLYEAR}/${CLASS_NAME}/team${TEAM_ID}/one50.git

# Update composer.json according to the user information
cd one50/app
sed -i "s/SCHOOLYEAR/${SCHOOLYEAR}/g" composer.template
sed -i "s/CLASS_NAME/${CLASS_NAME}/g" composer.template
sed -i "s/TEAM_ID/${TEAM_ID}/g" composer.template
cp composer.template composer.json

# Use composer to install project dependencies
composer install

# Setup database
mysql -uroot -pm150 < ../VM/initializeDatabase.sql

# Adjust database host since 'db' is the required configuration for docker usage
sed -i "s/db/localhost/g" Configuration/Settings.yaml

# Use flow/doctrine command to initialize database tables
./flow doctrine:create

# Import demo data
./flow shopdata:import

# Setup customer user
./flow user:createuser customer m150 $FIRST_NAME $LAST_NAME

# Setup admin user
./flow user:createadmin admin m150 $FIRST_NAME $LAST_NAME

# Configure Nginx
sudo cp ../VM/nginx.config /etc/nginx/sites-available/one50.local
sudo ln -sf /etc/nginx/sites-available/one50.local /etc/nginx/sites-enabled/
sudo service nginx restart

# Finish...
echo
echo
echo " =>  You may now access the One50 e-shop at http://one50.local"
echo
echo